# invertedonion

## A webapp that allows you to rename your images into a sorted sequence by drag and drop.

Why inverted onion? I took a picture of an onion that day and inverted it and it looked nice
https://mastodon.social/@qwazix/101213045574446701

## How to use?

Just clone it and doubleclick the html file.

A hosted version can be found [here](http://qwazix.com/shared/sorter.html) but it can go away at any time.

Nothing is uploaded to the server, everything happens in your browser. If you don't believe me check your browser's DevTools *Network* tab.

